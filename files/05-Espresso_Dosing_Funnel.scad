/*
    Description : Sphere in cube (basic OpenSCAD example)
    Author : Nicolas H.-P. De Coster (Vigon)
    Version :
        - 0.0 (draft)
    License : CC BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/)
    Notes : 
*/


$fn = 180*1;

// Base Outer diameter
OD = 64.5;

// Base Inner diameter
ID = 51.5;

// Funnel Diameter
// FD = 85;
FD = 85;

// Funnel Height
// FH = 18;
FH = 25;

// Overhang
OH = 0.5;

// thickness of walls
WALL = 1.8;

// thickness of lip
LIP = 1;

// LUG length
// LUGL = 27;
LUGL = 20.5;

// LUG number
LUGN = 3;

// LUG height
LUGH = 4;

// LUG TYPE: 1 == Normal, 2 == Inverted, 3 == Single inverted
// LUGT = 1;
LUGT = 1;

// Pusher margin
PM = 0.4;

// Pusher thickness
PTH = 2;

// Pusher grip diameter
PG = 6;

module body () {
    cylinder(d=OD, h=FH+LUGH);
    translate([0,0,LUGH]) cylinder(d1=FD*.75, d2=FD, h=FH);        
}

module lugholes () {
    if (LUGT == 1) { // Normal LUGs
        for(i=[0:(LUGN-1)]){
          rotate([0,0,i*360/LUGN])translate([0, OD/2, 0])
            cube([LUGL, OD, LUGH*2], center=true);
        }
        
    } else if (LUGT == 2) { // Inverted LUG
        translate([-OD/2-LUGL/2, 0, 0])
            cube([OD, OD*1.1, LUGH*2], center=true);
        translate([OD/2+LUGL/2, 0, 0])
            cube([OD, OD*1.1, LUGH*2], center=true);
    } else if (LUGT == 3) { // Single Inverted LUG
        translate([-OD/2-LUGL/2, 0, 0])
            cube([OD, OD*1.1, LUGH*2], center=true);
        translate([OD/2+LUGL/2, 0, 0])
            cube([OD, OD*1.1, LUGH*2], center=true);
        translate([0,OD/2,0])
            cube([OD, OD, LUGH*2], center=true);
    }
}



module funnel() {
  difference () {
    
    // main body of dosing funnel
    body();
    
    // cone hole
    translate([0,0,LUGH+OH+LIP]) cylinder(d1=ID, d2=FD-WALL/2, h=FH);

    // inner hole    
    translate([0,0,-FH*0.05]) cylinder(d=ID, h=LUGH+FH*1.1);
    
    // outer lip hole
    translate([0,0,-OH*0.05]) cylinder(d=OD-2*WALL, h=LUGH+OH*1.05);
    
    // LUG holes
    lugholes();
  }
}

module pusher() {
  translate([0,0, LUGH+OH])union(){
    cylinder(d = ID - 2*PM, h = PTH);
    cylinder(d1 = 2*PG, d2 = PG, h=PG);
    cylinder(d = PG, h = FH);
  }
}

funnel();
pusher();