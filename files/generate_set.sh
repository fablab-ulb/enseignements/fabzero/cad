#!/usr/bin/env bash

min=10
max=100
step=10


for i in $(seq $min $step $max); do 
  txt="obj_${i}.txt"
  stl="obj_${i}.stl"
  echo "Processing size $i"
  openscad -o "set/${stl}" -D "size=${i}" "${1}" &> "set/${txt}"
done 
