/*
    Description : Sphere in cube (basic OpenSCAD example)
    Author : Nicolas H.-P. De Coster (Vigon)
    Version :
        - 0.0 (draft)
    License : CC BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/)
    Notes : 
*/

eur_d   = 23.25;
eur_th  = 2.33;
hole    = 6;


e       = 0.01;
$fn     = 200;

module basic(){
  difference(){
    cylinder(d=eur_d, h=eur_th);
    translate([eur_d/4,0,-e])
        cylinder(d=hole, h=eur_th+2*e);
  }
}

module extended(){
  difference(){
    union(){
      translate([-eur_d/2,0,0])cylinder(d=eur_d, h=eur_th);
      minkowski(){
        cylinder(d=eur_d/4, h=eur_th/2);
        translate([0,0,eur_th/4])cube([eur_d, eur_d/4, eur_th/2], center=true);    
      }
    }
    translate([eur_d/2-hole/2,0,-e])
        cylinder(d=hole, h=eur_th+2*e);
  }
}

basic();
translate([0,eur_d, 0])
  extended();