/*
    Description : Sphere in cube (basic OpenSCAD example)
    Author : Nicolas H.-P. De Coster (Vigon)
    Version :
        - 0.0 (draft)
    License : CC BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/)
    Notes : 
*/

largeur    = 141;
hauteur    = 30;
epaisseur  = 20;
profondeur_texte = 1;
trou_vis_hauteur  = 50;
trou_vis_diametre = 4;
$fn=300;

$vpt=[-largeur/2,0,0];
rotate([-90,0,0]) {
difference(){
    rotate([0,270,0]) cylinder (largeur, d=epaisseur);
    translate([-largeur,0,-epaisseur/2]) cube([largeur,epaisseur/2,epaisseur]);
    rotate([90,0,0]) translate([0,0,hauteur-trou_vis_hauteur]) cylinder(h=trou_vis_hauteur, d=trou_vis_diametre);
    translate([-largeur,0,0]) rotate([90,0,0]) translate([0,0,hauteur-trou_vis_hauteur]) cylinder(trou_vis_hauteur, d=trou_vis_diametre);
    //texte("Angela O. E.");
}
rotate([90,0,0]) fixation();
translate([-largeur,0,0]) rotate([90,0,0]) fixation();
}

module fixation() {
    difference(){
    cylinder(hauteur,d=epaisseur);    
    translate([0,0,hauteur-trou_vis_hauteur]) cylinder(h=trou_vis_hauteur, d=trou_vis_diametre);
    translate([0,0,hauteur-trou_vis_hauteur-trou_vis_diametre]) cylinder(d1=0, d2=trou_vis_diametre, h=trou_vis_diametre);
    }
}

module texte(string) {
translate([-largeur/2,-profondeur_texte,0]) rotate([270,0,0]) linear_extrude(height=profondeur_texte, convexity=4)
text(string, 
     size=6,
     font="Bitstream Vera Sans",
     halign="center",
     valign="center");
    }
