/*
    Description : Sphere in cube (basic OpenSCAD example)
    Author : Nicolas H.-P. De Coster (Vigon)
    Version :
        - 0.0 (draft)
    License : CC BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/)
    Notes : 
*/

//high resolution rendering
$fn=200;

cubeSize = 20;
distance = 2;
sphereSize = 10;

//carved cube
difference(){
    cube(cubeSize, center=true);
    sphere(sphereSize+distance);
}

//sphere
sphere(sphereSize);