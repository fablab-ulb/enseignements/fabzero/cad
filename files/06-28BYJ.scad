/*
    File : 28BYJ.scad
    Author : Nicolas H.-P. De Coster (Vigon)
    Version (Date) :
        - 0.1 (2019-04-19)
    License : CC BY-NC-SA (https://creativecommons.org/licenses/by-nc-sa/4.0/)
    Notes : 
        - the "e" and "holes" parameters allows you to define an additive extra_dimension to facilitate mechanical insertion in case of "difference" use to put the motor inside another part
        - you can animate rotation in OpenScad (1turn/$t)
*/

//Calling the module
28BYJ(e_body=0,e_shaft_base=0, e_shaft=0, holes=true);

module 28BYJ(e_body=0, e_shaft_base=0, e_shaft=0, holes=true){
    $fn=100;
    epsilon = 0.1;
    //main
    d       = 28;
    h       = 19.3;
    deport  = 8;
    //baseshaft
    base_shaft_d = 9;
    base_shaft_h = 1.5;
    //shaft (from main plate)
    shaft_h = 10;
    shaft_d = 5;
    shaft_w = 3;
    shaft_cyl_h = 3.5;
    //lug
    lug_w       = 7;
    lug_h       = 0.8;
    lug_dist    = 35;
    lug_hole_d  = 4.2;
    //blue block
    to_center_depth = 17;
    block_h         = 17;
    block_w         = 14.6;
    sec_to_center_depth = 14;
    sec_block_w     = 17.5;
    
	union()
	{
		//Body
		translate([0,0,-(h+e_body)])color("silver") cylinder(d=d+e_body, h=h+e_body);
		//Base of motor shaft
		color("silver")translate([0,deport,0])cylinder(d=base_shaft_d+2*e_shaft_base, h=base_shaft_h+e_shaft_base);
        //Motor shaft
        color("gold")translate([0,deport,0])cylinder(d=shaft_d+2*e_shaft, h=shaft_cyl_h+e_shaft);
		color("gold")translate([0,deport,0])intersection(){
            cylinder(d=shaft_d+2*e_shaft, h=shaft_h+e_shaft);
            translate([0,0,(shaft_cyl_h+shaft_h+e_shaft)/2-epsilon])rotate([0,0,$t*360])cube([shaft_w+2*e_shaft,shaft_d+2*e_shaft,shaft_h-shaft_cyl_h+2*epsilon+e_shaft],center=true);
		}

        //lugs
        color("silver")translate([0,0,-lug_h])difference(){
            hull(){
                translate([-lug_dist/2,0,0])cylinder(d=lug_w+e_body, h=lug_h);
                translate([lug_dist/2,0,0])cylinder(d=lug_w+e_body, h=lug_h);
            }
            if(holes){
                translate([-lug_dist/2,0,-epsilon])cylinder(d=lug_hole_d-2*e_body,h=lug_h+2*epsilon);
                translate([lug_dist/2,0,-epsilon])cylinder(d=lug_hole_d-2*e_body,h=lug_h+2*epsilon);
             }
        }
        
        //Cable entry housing
		translate([0,0,-block_h/2-0.01-e_body/2])difference()
		{
			color("blue")union(){
                translate([0,-(to_center_depth+e_body)/2,0]) cube([block_w+2*e_body,to_center_depth+e_body,block_h+e_body],center=true);
                translate([0,-(sec_to_center_depth+e_body)/2,0]) cube([sec_block_w+2*e_body,sec_to_center_depth+e_body,block_h+e_body],center=true);
            }
			color("silver")cylinder(d=d, h=h+e_body, center=true);
		}
	}
}//end of stepper28BYJ module wrapper