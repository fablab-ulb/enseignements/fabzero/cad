/*
    Description : Sphere in cube (basic OpenSCAD example)
    Author : Nicolas H.-P. De Coster (Vigon)
    Version :
        - 0.0 (draft)
    License : CC BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/)
    Notes : 
      - Key code is provided by "cylinders" quadruplet
*/


cylinders = [1,2,1,2];

body_size       = 18;
body_round_rad  =  5;
body_hole_dia   =  5; 
key_lenght      = 24;
key_width       =  8;
key_depth       =  2;

code_lenght     = 18;
code_width      =  5.80;


rail_depth      = 1;
rail_width1     = 2;
rail_distance1  = 0.6;
rail_width2     = 2.2;
rail_distance2  = 3.8;

cylinder_width  = 1.4;
cylinders_distance = 2.4;
cylinder_first_position = 14; //(from the end of the key)

end_height      = 6; //from the bottom line

$vpr=[360*$t,720*$t,0];
$vpt=[0,0,0];
$fn=200;


difference(){
    //key body
    union(){
        translate([key_lenght/2,0,0]) cube([key_lenght,key_width,key_depth],center=true);
        translate([-body_size/2,0,0]) rounded(body_size,body_round_rad,key_depth);
    }
    
    //rails
    translate([key_lenght/2,(key_width-rail_width1)/2-rail_distance1, (key_depth-rail_depth)/2]) cube([key_lenght,rail_width1,rail_depth],center=true);
    translate([key_lenght/2,(key_width-rail_width2)/2-rail_distance2, (key_depth-rail_depth)/2]) cube([key_lenght,rail_width1,rail_depth],center=true);
    
    //cut bottom code
    translate([key_lenght-code_lenght/2,-code_width/2,0]) cube([code_lenght, key_width-code_width, key_depth],center=true);
    
    //plots
    for(i=[0:3]){
          translate([i*cylinders_distance,cylinders[i],0]) plot();        
        }
    
    //End point    
    translate([key_lenght,-key_width*(1+sqrt(2))/2+end_height,0]) rotate([0,0,45]) cube([key_lenght,key_width,key_depth],center=true);
    translate([key_lenght,-key_width*(1-sqrt(2))/2+end_height,0]) rotate([0,0,-45]) cube([key_lenght,key_width,key_depth],center=true);
    
    //hole
    translate([-body_size*3/4,0,0]) cylinder(d=body_hole_dia,h=key_depth,center=true);
}

module rounded(size,r,h){
    minkowski(){
    cube([size-2*r,size-2*r,h/2],center=true);
    cylinder(r=r,h=h/2,center=true);}
}

module plot(){
    translate([key_lenght-cylinder_first_position,(key_width-3*code_width)/2,0])
    difference(){
    cube([2*cylinders_distance, code_width, key_depth],center=true);
    translate([-code_width/2,(cylinder_width+2*sqrt(2)*cylinders_distance)/2,0]) rotate([0,0,-45]) cube([2*cylinders_distance, code_width, key_depth],center=true);
    mirror([1,0,0])
        translate([-code_width/2,(cylinder_width+2*sqrt(2)*cylinders_distance)/2,0]) rotate([0,0,-45]) cube([2*cylinders_distance, code_width, key_depth],center=true);
    }
}