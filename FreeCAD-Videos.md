+++
title = "FreeCAD"
author = "Nicolas H.-P. De Coster (Vigon)"
+++

# FreeCAD

![](img/OpenSCAD_FreeCAD.mp4)

![](img/01-sketch.mp4)

![](img/02-pad.mp4)

![](img/03-new_sketch.mp4)

![](img/04-filet.mp4)


## Tutorials

- [Aalto Fablab](https://www.youtube.com/@aaltofablab3504/videos)
- [@AdrewCAD](https://www.youtube.com/@AndrewCAD/videos)
