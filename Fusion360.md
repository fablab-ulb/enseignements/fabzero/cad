# AutoDesk - Fusion 360

## Licences:
s'inscrire [ici](https://www.autodesk.com/support/technical/article/caas/sfdcarticles/sfdcarticles/How-to-activate-start-up-or-educational-licensing-for-Fusion-360.html)
pour obtenir une licence de Fusion 360

## Official Documentation

[Fusion Product Documentation](https://help.autodesk.com/view/fusion360/ENU/?guid=GUID-1C665B4D-7BF7-4FDF-98B0-AA7EE12B5AC2)
[Training/Learning resources for Autodesk Fusion](https://www.autodesk.com/support/technical/article/caas/sfdcarticles/sfdcarticles/Training-Learning-resources-for-Fusion-360.html)
