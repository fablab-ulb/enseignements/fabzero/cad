+++
title = "Computer Aided Design"
author = "Nicolas H.-P. De Coster (Vigon) & Patrick Dezséri"
+++

# Computer-Aided Design

Training modules about Computer Aided Design (CAD).


## Softwares

- [Inkscape](./Inkscape.md)
- [OpenSCAD](./OpenSCAD.md)
- [FreeCAD](./FreeCAD.md)
- **Many** others... :
  - [Antimony](https://github.com/mkeeter/antimony) 
  - [Fusion360](https://www.autodesk.com/products/fusion-360/)
  - Rhino/[Grasshopper](https://www.grasshopper3d.com/)
  - [Onshape](https://www.onshape.com/products/education)


Pour nous aider a améliorer nos tutorielle vous pouvez le faire directement via GitLab ou en nous envoyant un mail a fablab@ulb.be