+++
title = "Inkscape"
author = "Nicolas H.-P. De Coster (Vigon)"
+++

# Inkscape

- Raster VS vector : https://en.wikipedia.org/wiki/Vector_graphics
- [SVG](https://fr.wikipedia.org/wiki/Scalable_Vector_Graphics) : try to "read text".
- Tutorials : https://inkscape.org/learn/tutorials/
