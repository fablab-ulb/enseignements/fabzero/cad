+++
title = "FreeCAD"
author = "Dezséri Patrik"
coauthors = ""
+++

# FreeCAD

## The Spreadsheet

The spreadsheet is used to create a parametric object.  
But what should we understand under parametric object ? The following citation explains it pretty well.

`It does not represent objects with fixed geometry and properties. 
Rather, it represents objects by parameters and rules that determine the geometry as well as some non-geometric properties and features.
The parameters and rules allow the objects to automatically update according to user control or changing contexts.`  
*[BIM Handbook](http://staff.www.ltu.se/~marsan/P7006B/Literature/Eastman%20et%20al%20-%20BIM%20handbook/ch2.pdf)*
*: A Guide to Building InformationModeling for Owners, Managers,Designers, Engineers, and Contractors. Chuck Eastman, Paul Teicholz, Rafael Sacks and Kathleen Liston Copyright © 2008 John Wiley & Sons, Inc*

![Fig1](img/FreeCAD/excel-1.png){width="450"}  
**Fig 1**

In the dropdown menu you can select the Spreadsheet page (*orange bullet 1*).  
To create a new Spreadsheet you just need to click the Spreadsheet logo under the *green arrow 2*.  

*Purple arrow 3* allows you to change the color of the cases or the letters.  
Note that the **faint yellow** has a **special meaning** (see later) and not a user made coloration.

![Fig2](img/FreeCAD/excel-2.png){width="450"}  
**Fig 2**

Each person will have a different layout of usage of the Spreadsheet.  
Mine is as follows:

* First column is a description of the parameter
* Second column is the alias used for the parameter
* Third column is the value of the parameter itself
* Forth column is the unit

Lets come back to the meaning of [<u>*alias*</u>](https://wiki.freecad.org/Spreadsheet_SetAlias).
Instead of referring to just a cell (eg: C4) when we will give a value for our 3D design, we will give him a cell reference with an alias (eg: LBW).
We can give this alias in the *Alias* cell just under the *blue arrow 4*.
This allows us, in the case if we need one more column or row, to just insert a new row/column where needed, without needing to rerefer the new cell in our 3D design.
It is also easier to remember an acronym (=alias) than a cell number.  
**When an alias is given to a cell it will have a faint yellow background coloration.**

We can have multiple spreadsheet within the same FreeCAD project.

## Part Designer

### Step 1 - Creating an object

Selecting the *Part Designer* in the dropdown menu we can create an new object using the *create body* button (looks like a blue chair/stair).  
This can also be done in the *Task* window next to the *Model* window.

* The model window shows all the sub files that are present in our FreeCAD project. All the spreadsheet, 3D object (be it imported or created by yourself) etc.
* The task window shows what needs to be done or what can still be done.

![Fig3](img/FreeCAD/body.png){width="450"}  
**Fig 3**

We can have multiple bodies in a FreeCAD project.

### Step 2 - Creating a Sketch

After creating the body we will need to create a sketch.
It can be done by two ways.
We can click on *Create sketch* in the Task window or we can click on the *Create sketch* button, which can be find next to the *create body* button (still in the Part Designer menu).

![Fig4](img/FreeCAD/create-sketch.png){width="350"}  
**Fig 4**

Next we can choose a plane in which we want to work.

![Fig5](img/FreeCAD/plane.png){width="350"}  
**Fig 5**

## Sketcher

### Tools

[comment]: # (not sure if this is the best title)

There are two type of tools that we will be using.

![Fig6](img/FreeCAD/tools.png){width="450"}  
**Fig 6**

The first is the **geometrical** tools (orange - 1) with which we can "draw".
For example we have the line, circle, rectangle tools. (There are more)

The second type are the **constrains** tools (green - 2) which help define our drawing/object.
For example we have the horizontal, distance, angle constrains.
It is the usage of constrains that will allow us to create a parametric object.
It is also with the help of constraint that we can have a drawing with 0 degree of freedom (DoF).

### Degree of Freedom - DoF

An important notion is the DoF.

`In mechanics, degrees of freedom (DOF) is the number of independent variables that define the possible positions or motions of a mechanical system in space. DOF measurements assume that the mechanism is both rigid and unconstrained, whether it operates in two-dimensional or three-dimensional space.`  
Source: [TechTarget](https://www.techtarget.com/whatis/definition/degrees-of-freedom)

In FreeCAD we always draw / design in 2D.
Even if we are modifying a 3D object we are working only on a face of that said object.

A few example of different geometrical objects DoF.

|       | Point      | Circle | Line     |
|:----: | :----:      |    :----:   |  :----:       |
|   DoF: | 2      | 3       | 4   |
|more in depth| X coordinate <br> Y coordinate | X and Y coordinates of the center point of the circle <br> The radius of the circle| X and Y coordinates of each end points of the line|

What we want is that our drawings have a DoF of 0.
That means that it has been fully constrained.

### Let's start drawing

We will first use the line tool to to create an approximate shape of the chair.
**When any of the tools is selected, the symbol of that will show up next to the mouse.**
While using this tool, sometimes it will propose to directly add a vertical/horizontal constrain.
You can also start a line on an already existing point.

![Fig7](img/FreeCAD/sketch1.png){width="450"}  
**Fig 7**

If you missed to make some of the line horizontal or vertical, you can select the appropriate constrain tool.
After the symbol showed up next to the mouse, you can select the two points that delimits the line that needs to be vertical/horizontal.
When a point (or line) is selected it will change color to dark green.  
**Note: the vertical and horizontal is always referenced with the x/y/z axis.**  
If two points should be one, you can select the *coincidence* constrain tool.
Select the two point which should be one and FreeCAD will put them together.  
**Make sure that on of the points of your drawing coincide with the 0,0 central point of the x/y/z axis.**

![Fig8](img/FreeCAD/sketch2.png){width="450"}  
**Fig 8**

You can also use the parallel tool to make two lines parallel.
When a point (or line) is selected it will change color to dark green.

### Distance constrain

Using the distance constrain, we can select **any** two points and define a distance between them.
These two points can define a line, a radius or just two random points (0,0 central point included).

![Fig9](img/FreeCAD/distance.png){width="450"}  
**Fig 9**

After two points have been selected a popup will appear asking for the distance that we want.
We can give him a fix distance there or by clicking the f(x) blue circle, we can give him a formula.
In this formula cell we can reference a cell from our spreadsheet.
We can use either a cell number or an alias given to a cell.
This allows us to have a parametric distance and so a parametric object.

### Additional info on constrains

On the left side panel in Task windows you can see a *constrains* windows.
This list all the constrains you have in your sketch.
You can delete them from there or from the sketch when selecting the unwanted constrains symbol on the drawing.
If you have too many constrains on the screen (generally distance constrains) you can hide/show them from this window.

### Making it 3D

Before making our drawing a 3D object we need it to be fully constrained.
To know if it is or not we can look at the left side panels and search for the **Solver messages** int the task window.
For example you can see in Fig 7 and Fig 8 that they have respectively 14 and 6 DoF(s).  
**When a sketch is fully constrained it will be written in the Solver messages *Fully constrained* and the drawing will be colored a bright (neon) green.**

Exiting the sketch mode and choosing the Part Design menu from the dropdown menu we can make our sketch a 3D object.
For this we will be using the **Pad** tool (orange arrow Fig 10).
A menu will popup asking us for the depth/width of the object that we want.
Of course we will be doing the same process as previously to make it a parametric.

![Fig10](img/FreeCAD/Pad.png){width="450"}  
**Fig 10**

### Modifying a 3D object

Going forward I will presume that the spreadsheet has already all the distances and aliases set for all the follow up modifications or objects.

We first need to select the face on which we want to work / the face we want to modify.
Than in the Part Design menu we can select *Create Sketch* to create a new sketch that is linked to that face.

#### Removing material - Pocket

Than we can create the sliding holes sketch where we want them.
In this case we can use the rectangular tool as each slides should be parallel to each other and to the axis.
As you can see in Fig 11 we can simply make the rectangle bigger than the base 3D object.
For simplicity I only created 5 slides.

![Fig11](img/FreeCAD/slides.png){width="450"}  
**Fig 11**

When we finished the sketch of the slides we can close the sketch and go back to the Part Design menu where we will choose the *Pocket* tool to create the holes.
When asked we can give a depth that should not be (equal or) bigger than the depth of the original object (we do not want a real hole).

#### Removing material - Hole

[comment]: # (explain difference between hole and pocket tool)

Next we do the same steps as before to create the holes that will be used for the metal bars.
In this case we want to give the same (or bigger) depth than the depth of the original object to create a passthrough hole.

### Rectangle with an angle 

Next we will create the back rests holes.
The difficulty with this pocket is that it is at an angle.
For this we will use two new tools.
First we will create a rectangle.
You can use the *create rectangle* tool or just draw a rectangle with the *create line* tool.
When we have our rectangle we will use the first new tool, the *constrain distance* tool.
With this we can give a distance between two points without the additional horizontal/vertical constraint of the *constraint horizontal/vertical distance* tools.

If you used the *create rectangle* tool (and it might also be the case if you used the line tool) we need to delete the horizontal and vertical constriants.
We will replace them using the second new tool, the *constraint angle* tool.
Select the tool, than select two side and give and angle of 90°.
Do it for 3 corners.

To finish you need to give a *constraint horizontal* and *vertical distance* from the 0,0 pont and also an *angle* constraint from one of the main axis.

![Fig12](img/FreeCAD/backrest.png){width="450"}  
**Fig 12**

### Mirror

When we are done with one of the leg of the Trip-Trap chair, we are going into the *Part* menu from the dropdown menu.
There we first select our 3D object (our leg) and than select the *Mirroring...* tool.
Than we select the body from the left side panel that we want to mirror and the plan in which we want to mirror it.

After the mirroring we can right click in the Model windows on the mirror image (or the original object) and select the *transform* option.
Choose the adequate arrow to move the 3D object.

![Fig13](img/FreeCAD/mirror.png){width="450"}  
**Fig 13**

## Creating a 2nd body

In the Part Designer, you can click on create body to create a 2nd (3rd, ...) body in your current project.
Do not forget to rename it so you wont mix up who is who in the project.
By right clicking on it you can again use transform to move it around visually.
You can also change its color with the right click.
You can give it a special color with *Appearance* or a random color with *Random color*.

Important to note that only the **active body** can be modified.
To activate a body, double click on it.
When a body is active, its name will be listed in **bold**.

If there is too many bodies on the screen, select the unnecessary bodies (no need to activate them) and press the space-bar to visually hide it.

## Aesthetics 

### Fillet and Chamfer

Each objects thus far created have sharpe edges.
To smooth it we have 2 tools.
Both of them can be find in the *Part Designer* menu.
Fillet to make it curved and chamfer to flatten the edge.

![Fig15](img/FreeCAD/chamfer.png){width="450"}  
**Fig 15**

Select an edge (use ctrl to select multiples at the same time) and than selecting the *fillet* tool (or chamfer) to make it smoother.
We can play on the value of radius (for fillet, size for chamfer) to make it bigger or smaller.

Of course this can have also a technological porpose and not just an aesthetics one. 
For example, having a guiding slope.

## Exporting into STL

First, select the 3D object that you want to export for 3D printing.
Than in the *File* menu select export.
The shortcut for exporting is *Ctrl+E*.
Make sure to select the **STL Mesh** file format.
Choose where you want to export it.
When you are satisfied with the save location hit *Save*.
You can now import your STL file into the Slicer of the 3D printer.

NB:

* We never specified if our body is 100% full or is it just an empty shell.
This detail is specified in the slicer of the 3D printer.

## Exporting into SVG

This is only needed if you want to cut your object out of something.
It is useful with laser cutters and CNC.

For this we will need a plugin.

Also, you might need to convert some depth of pockets to make it passthrought the hole object so it's shadow can be detected by the LCInterlocking module.
Further corrections might need to be added with InkScape.

### Plugin install

In the *Tools* menu select the *Addon manager*.
Search for *LCInterlocking* and install it.

After installing it it should appear in the dropdown menu where you have Part Designer and Sketcher.

### Exporting

Select the *Laser Cut Interlocking* from the dropdown menu (where you previously found Part Designer and Sketcher).
Than select the body (or bodies) that you want to export into and SVG file.
Use the export tool (in the tool bar and not from files) to create a new file with the SVG "shadow".

![Fig14](img/FreeCAD/SVG.png){width="450"}  
**Fig 14**

Select the *Shape2DView* layer and from the *File* menu select export.
This time you want to select the **Flattened SVG** file format.
Open it in InkScape and make sure to convert the objects (the lines) to path.
It is located in the *Path* menu.
Otherwise the laser cutters or CNCs might not recognize the shape and wont cut it out.

Further steps that I sometimes need to do (taking an example with square).
I select the fours sides and than use the *Combine* tool (ctrl+K) from the *Path* dropdown menu.
Than I reselect the square and hit F2 on my keyboard.
Than I select one of the corner and use the *Join selected endnodes* (shift+J).


# More Resources

Learning FreeCAD [YouTube Channel](https://www.youtube.com/@learningfreecad8036)

FreeCAD [Wiki](https://wiki.freecad.org/Main_Page)

[LCInterlocking](https://github.com/execuc/LCInterlocking)
 wiki

